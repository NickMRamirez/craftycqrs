﻿
using CraftyCqrs.Framework;

namespace CraftyCqrs.IntegrationTests.SampleCommands
{
    public class SampleCommand2 : Command
    {
        public string TestProperty1 { get; set; } 
    }
}