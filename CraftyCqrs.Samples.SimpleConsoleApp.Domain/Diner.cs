﻿
using CraftyCqrs.Framework;
using CraftyCqrs.Framework.Composition;
using System.Collections.Generic;

namespace CraftyCqrs.Samples.SimpleConsoleApp.Domain
{
    // The command...
    public class OrderFrenchToastCommand : Command
    {
        public OrderFrenchToastCommand(int toastNumber, bool addSyrup, string instructions)
        {
            this.SlicesOfToastNumber = toastNumber;
            this.AddSyrup = addSyrup;
            this.SpecialInstructions = instructions;
        }

        public int SlicesOfToastNumber { get; private set; }
        public bool AddSyrup { get; private set; }
        public string SpecialInstructions { get; private set; }
    }

    // The command handler...
    [Handles("CraftyCqrs.Samples.SimpleConsoleApp.Domain.OrderFrenchToastCommand")]
    public class OrderFrenchToastCommandHandler : CommandHandler
    {
        public override void Execute(dynamic command)
        {
            DinerDomainModelAggregate.AddFrenchToastOrder(new Order()
            {
                ToastSlices = command.SlicesOfToastNumber,
                Syrup = command.AddSyrup,
                SpecialInstructions = command.SpecialInstructions
            });
        }
    }

    // The Domain model...
    public class DinerDomainModelAggregate
    {
        static DinerDomainModelAggregate()
        {
            Orders = new List<Order>();
        }

        public static IList<Order> Orders { get; private set; }

        public static void AddFrenchToastOrder(Order order)
        {
            Orders.Add(order);
        }
    }

    public struct Order
    {
        public int ToastSlices;
        public bool Syrup;
        public string SpecialInstructions;
    }
}
