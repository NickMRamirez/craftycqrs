﻿
using NUnit.Framework;
using CraftyCqrs.Framework.Composition;

namespace CraftyCqrs.Tests
{
    [TestFixture]
    public class HandlesAttributeTests
    {
        [Test]
        public void ConstructorSetsCommandHandledPropertyWithGivenString()
        {
            // arrange
            string nameOfCommand = "SomeNamespace.SomeCommand";

            // act
            var attribute = new HandlesAttribute(nameOfCommand);

            // assert
            Assert.AreEqual(nameOfCommand, attribute.CommandHandled);
        }
    }
}