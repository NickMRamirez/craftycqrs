﻿
using CraftyCqrs.Framework;
using CraftyCqrs.Framework.Composition;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace CraftyCqrs.Tests
{
    [TestFixture]
    public class CommandRegistryTests
    {
        [Test]
        public void ConstructorInitializesHandlersCollection()
        {
            // arrange
            var mockCommandFinder = new Mock<ICommandClassFinder>();
            var mockCommandHandlerFinder = new Mock<ICommandHandlerClassFinder>();
            
            // act
            var registry = new CommandRegistry(mockCommandFinder.Object, mockCommandHandlerFinder.Object);

            // assert
            Assert.IsNotNull(registry.Handlers);
        }
    }
}