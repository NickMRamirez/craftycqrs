﻿
using CraftyCqrs.Framework;
using Moq;
using NUnit.Framework;

namespace CraftyCqrs.Tests
{
    [TestFixture]
    public class CommandEnvelopeTests
    {
        [Test]
        public void ConstructorSetsCommandPropertyWithGivenCommand()
        {
            // arrange
            var commandMock = new Mock<Command>();

            // act
            var envelope = new CommandEnvelope(commandMock.Object);

            // assert
            Assert.AreEqual(commandMock.Object, envelope.Command);
        }
    }
}