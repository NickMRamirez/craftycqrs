﻿
using CraftyCqrs.Framework.Composition;
using NUnit.Framework;
using System.Linq;

namespace CraftyCqrs.IntegrationTests
{
    [TestFixture]
    public class CommandClassFinderTests
    {
        private CommandClassFinder finder;
        
        [SetUp]
        public void Setup()
        {
            this.finder = new CommandClassFinder();
        }
        
        [Test]
        public void GetCommandTypesReturnsSampleCommandTypes()
        {
            var commandTypes = this.finder.GetCommandTypes().ToList();
            string type1 = "CraftyCqrs.IntegrationTests.SampleCommands.SampleCommand1";
            string type2 = "CraftyCqrs.IntegrationTests.SampleCommands.SampleCommand2";

            Assert.IsTrue(commandTypes.Any(t => t.FullName == type1));
            Assert.IsTrue(commandTypes.Any(t => t.FullName == type2));
        }
    }
}