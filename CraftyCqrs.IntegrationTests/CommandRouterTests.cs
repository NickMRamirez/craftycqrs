﻿
using CraftyCqrs.Framework;
using CraftyCqrs.IntegrationTests.SampleCommands;
using CraftyCqrs.IntegrationTests.SampleDomainModel;
using NUnit.Framework;

namespace CraftyCqrs.IntegrationTests
{
    [TestFixture]
    public class CommandRouterTests
    {
        private CommandRouter router;

        [SetUp]
        public void Setup()
        {
            this.router = new CommandRouter();
        }

        [Test]
        public void SendTriggersExecuteOnCommandHandler1()
        {
            // arrange
            var envelope = new CommandEnvelope(new SampleCommand1());

            // act
            this.router.Send(envelope);

            // assert
            Assert.AreEqual("test value", SampleDomainModelClass.StringToSet);
        }

        [Test]
        public void SendTriggersExecuteOnCommandHandler2()
        {
            // arrange
            var envelope = new CommandEnvelope(new SampleCommand2()
            {
                TestProperty1 = "ABC123"
            });

            // act
            this.router.Send(envelope);

            // assert
            Assert.AreEqual("ABC123", SampleDomainModelClass.StringToSet);
        }
    }
}
