﻿using CraftyCqrs.Framework;
using CraftyCqrs.Framework.Composition;
using CraftyCqrs.IntegrationTests.SampleCommands;
using CraftyCqrs.IntegrationTests.SampleDomainModel;
using NUnit.Framework;
using System;
using System.Linq;

namespace CraftyCqrs.IntegrationTests
{
    [TestFixture]
    public class CommandRegistryTests
    {
        private CommandRegistry registry;

        [SetUp]
        public void Setup()
        {
            this.registry = new CommandRegistry();
        }

        [Test]
        public void GetAssociatedCommandRetrievesCommandNameFromHandlesAttribute()
        {
            // arrange
            var handlerFinder = new CommandHandlerClassFinder();
            var handlerTypes = handlerFinder.GetCommandHandlerTypes();

            // act
            string commandName = this.registry.GetAssociatedCommand(handlerTypes.FirstOrDefault());

            // assert
            Assert.AreEqual("CraftyCqrs.IntegrationTests.SampleCommands.SampleCommand1", commandName);
        }

        [Test]
        public void GetHandlerForCommandReturnsHandlerForCommandSpecifiedByHandlesAttribute()
        {
            // arrange
            var commandFinder = new CommandClassFinder();
            var commandTypes = commandFinder.GetCommandTypes();
            var handlerFinder = new CommandHandlerClassFinder();
            var handlerTypes = handlerFinder.GetCommandHandlerTypes();

            // act
            var handler = this.registry.GetHandlerForCommand(commandTypes.First(), handlerTypes);

            // assert
            Assert.AreEqual("SampleCommandHandler1", handler.GetType().Name);
        }

        [Test]
        public void RegisterHandlersInReflectedAssembliesSetsHandlesProperty()
        {
            // act
            this.registry.RegisterHandlersInReflectedAssemblies();

            // assert
            Assert.AreEqual(2, this.registry.Handlers.Count);
        }

        [Test]
        public void CallingExecuteOnHandlerExecutesExpectedCode()
        {
            // arrange
            this.registry.RegisterHandlersInReflectedAssemblies();

            // act
            this.registry.Handlers.First().Value(new CommandEnvelope(new SampleCommand1()));

            // assert
            Assert.AreEqual("test value", SampleDomainModelClass.StringToSet);
        }
    }
}