﻿
using CraftyCqrs.Framework.Composition;
using NUnit.Framework;
using System.IO;
using System.Linq;

namespace CraftyCqrs.IntegrationTests
{
    [TestFixture]
    public class AssemblyFinderTests
    {
        private IAssemblyFinder finder;
        
        [SetUp]
        public void Setup()
        {
            this.finder = new AssemblyFinder();
        }
        
        [Test]
        public void GetAssembliesReturnsExpectedSampleAssemblies()
        {
            var assemblies = this.finder.GetAssemblies().ToList();
            Assert.Contains(
                Path.Combine(Directory.GetCurrentDirectory(), "Commands", "CraftyCqrs.IntegrationTests.SampleCommands.dll"),
                assemblies);
        }
    }
}
