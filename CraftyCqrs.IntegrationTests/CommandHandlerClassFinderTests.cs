﻿
using CraftyCqrs.Framework.Composition;
using NUnit.Framework;
using System.Linq;

namespace CraftyCqrs.IntegrationTests
{
    [TestFixture]
    public class CommandHandlerClassFinderTests
    {
        private CommandHandlerClassFinder finder;

        [SetUp]
        public void Setup()
        {
            this.finder = new CommandHandlerClassFinder();
        }

        [Test]
        public void GetCommandHandlerTypesReturnsSampleCommandTypes()
        {
            var commandHandlerTypes = this.finder.GetCommandHandlerTypes().ToList();
            string type1 = "CraftyCqrs.IntegrationTests.SampleCommandHandlers.SampleCommandHandler1";
            string type2 = "CraftyCqrs.IntegrationTests.SampleCommandHandlers.SampleCommandHandler2";

            Assert.IsTrue(commandHandlerTypes.Any(t => t.FullName == type1));
            Assert.IsTrue(commandHandlerTypes.Any(t => t.FullName == type2));
        }
    }
}
