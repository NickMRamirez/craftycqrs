﻿
using CraftyCqrs.Framework.Composition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CraftyCqrs.Framework
{
    public class CommandRegistry : ICommandRegistry
    {
        private ICommandClassFinder commandClassFinder;
        private ICommandHandlerClassFinder commandHandlerClassFinder;

        /// <summary>
        /// Initializes a new instance of the CommandRegistry class.
        /// </summary>
        public CommandRegistry() : this(new CommandClassFinder(), new CommandHandlerClassFinder())
        {
        }

        /// <summary>
        /// Initializes a new instance of the CommandRegistry class.
        /// </summary>
        /// <param name="commandClassFinder">Instance of command class finder</param>
        /// <param name="commandHandlerClassFinder">Instance of command handler class finder</param>
        public CommandRegistry(ICommandClassFinder commandClassFinder, ICommandHandlerClassFinder commandHandlerClassFinder)
        {
            this.commandClassFinder = commandClassFinder;
            this.commandHandlerClassFinder = commandHandlerClassFinder;
            this.Handlers = new Dictionary<string, Action<CommandEnvelope>>();
        }

        /// <summary>
        /// Gets the dictionary of command types and their associated handlers
        /// </summary>
        public IDictionary<string, Action<CommandEnvelope>> Handlers { get; private set; }

        /// <summary>
        /// Registers all command handlers in assemblies found through reflection
        /// in the current directory and its subdirectories
        /// </summary>
        public void RegisterHandlersInReflectedAssemblies()
        {
            IEnumerable<Type> allCommands = this.commandClassFinder.GetCommandTypes().Distinct();
            IEnumerable<Type> allHandlers = this.commandHandlerClassFinder.GetCommandHandlerTypes().Distinct();

            this.Handlers.Clear();

            foreach (Type cmdType in allCommands)
            {
                if (this.Handlers.Any(h => h.Key == cmdType.FullName))
                {
                    continue; // don't duplicate keys
                }
                
                this.Handlers.Add(cmdType.FullName, envelope =>
                {
                    var handler = this.GetHandlerForCommand(cmdType, allHandlers);

                    if (handler == null)
                    {
                        throw new UndefinedCommandException(cmdType.FullName);
                    }

                    handler.Execute(envelope.Command);
                });
            }
        }

        /// <summary>
        /// Gets the handler for a given type of command
        /// </summary>
        /// <param name="commandType">The type of command to find a handler for</param>
        /// <param name="commandHandlers">A collection of all available handlers</param>
        /// <returns>The handler corresponding to the given type of command</returns>
        internal CommandHandler GetHandlerForCommand(Type commandType, IEnumerable<Type> commandHandlers)
        {
            // find based on HandlesAttribute
            var foundHandler = (from handlerType in commandHandlers
                               where this.GetAssociatedCommand(handlerType).ToString() == commandType.FullName 
                               select handlerType).FirstOrDefault();

            if (foundHandler == null)
            {
                return null; // TODO: Fall back to another method of finding handler?
            }

            return (CommandHandler)Activator.CreateInstance(foundHandler);
        }

        /// <summary>
        /// Gets the command associated with the given type of command handler
        /// </summary>
        /// <param name="commandHandler">The Type of a command handler</param>
        /// <returns>The name of the command class</returns>
        internal string GetAssociatedCommand(Type commandHandler)
        {
            var attr = commandHandler.GetCustomAttribute<HandlesAttribute>(true);

            if (attr == null)
            {
                return string.Empty;
            }

            return attr.CommandHandled;
        }
    }
}