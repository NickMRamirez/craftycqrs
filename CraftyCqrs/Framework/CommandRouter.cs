﻿
using System;
using System.Linq;

namespace CraftyCqrs.Framework
{
    public class CommandRouter
    {
        private ICommandRegistry commandRegistry;

        public CommandRouter() : this(new CommandRegistry())
        {
        }

        public CommandRouter(ICommandRegistry commandRegistry)
        {
            this.commandRegistry = commandRegistry;
            this.commandRegistry.RegisterHandlersInReflectedAssemblies();
        }
        
        public void Send(CommandEnvelope envelope)
        {
            Action<CommandEnvelope> action;
            Type commandType = envelope.Command.GetType();

            if (this.commandRegistry.Handlers.TryGetValue(commandType.FullName, out action))
            {
                action(envelope);
            }
            else
            {
                throw new UndefinedCommandException(envelope.Command.ToString());
            }
        }
    }
}