﻿
namespace CraftyCqrs.Framework
{
    public abstract class CommandHandler
    {
        /// <summary>
        /// Handles the given command, reading its properties
        /// and then calling logic in the domain model
        /// </summary>
        /// <param name="command">The command to handle</param>
        public abstract void Execute(dynamic command);
    }
}