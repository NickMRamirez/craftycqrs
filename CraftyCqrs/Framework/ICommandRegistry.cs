﻿using System;
using System.Collections.Generic;

namespace CraftyCqrs.Framework
{
    public interface ICommandRegistry
    {
        IDictionary<string, Action<CommandEnvelope>> Handlers { get; }
        void RegisterHandlersInReflectedAssemblies();
    }
}
