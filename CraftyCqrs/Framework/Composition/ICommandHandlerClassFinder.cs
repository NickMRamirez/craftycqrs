﻿using System;
using System.Collections.Generic;

namespace CraftyCqrs.Framework.Composition
{
    public interface ICommandHandlerClassFinder
    {
        /// <summary>
        /// Gets Type objects for CommandHandler classes
        /// </summary>
        /// <returns>A collection of Type objects for CommandHandler classes</returns>
        IEnumerable<Type> GetCommandHandlerTypes();
    }
}