﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CraftyCqrs.Framework.Composition
{
    internal interface IAssemblyFinder
    {
        /// <summary>
        /// Gets the assemblies within the current directory and all subdirectories
        /// </summary>
        /// <returns>The assemblies within the current directory and all subdirectories</returns>
        IEnumerable<string> GetAssemblies();
    }
}