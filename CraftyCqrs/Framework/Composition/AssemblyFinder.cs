﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CraftyCqrs.Framework.Composition
{
    internal class AssemblyFinder : IAssemblyFinder
    {
        /// <summary>
        /// Gets the assemblies within the current directory and all subdirectories
        /// </summary>
        /// <returns>The assemblies within the current directory and all subdirectories</returns>
        public IEnumerable<string> GetAssemblies()
        {
            return Directory.GetFiles(
                AppDomain.CurrentDomain.BaseDirectory, 
                "*.dll", 
                SearchOption.AllDirectories).ToList();
        }
    }
}