﻿using System;
using System.Collections.Generic;

namespace CraftyCqrs.Framework.Composition
{
    public interface ICommandClassFinder
    {
        /// <summary>
        /// Gets Type objects for Command classes
        /// </summary>
        /// <returns>A collection of Type objects for Command classes</returns>
        IEnumerable<Type> GetCommandTypes();
    }
}