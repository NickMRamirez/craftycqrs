﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CraftyCqrs.Framework.Composition
{
    /// <summary>
    /// Finds classes that derive from Command
    /// </summary>
    internal class CommandClassFinder : ICommandClassFinder
    {
        private IAssemblyFinder assemblyFinder;

        /// <summary>
        /// Initializes a new instance of the CommandClassFinder class.
        /// </summary>
        public CommandClassFinder() : this(new AssemblyFinder())
        {
        }
        
        /// <summary>
        /// Initializes a new instance of the CommandClassFinder class.
        /// </summary>
        /// <param name="assemblyFinder">An instance of IAssemblyFinder for finding assemblies
        /// in the current directory and its subdirectories</param>
        public CommandClassFinder(IAssemblyFinder assemblyFinder)
        {
            this.assemblyFinder = assemblyFinder;
        }
        
        /// <summary>
        /// Gets a collection of command types exposed by the 
        /// asemblies in the current directory and all subdirectories
        /// </summary>
        /// <returns>A collection of command types</returns>
        public IEnumerable<Type> GetCommandTypes()
        {
            var assemblies = this.assemblyFinder.GetAssemblies();

            foreach (var assm in assemblies)
            {
                Assembly loadedAssembly = Assembly.LoadFile(assm);

                var commands =
                    from type in loadedAssembly.ExportedTypes
                    where typeof(Command).IsAssignableFrom(type)
                    select type;

                foreach (var command in commands)
                {
                    yield return command;
                }
            }
        }
    }
}