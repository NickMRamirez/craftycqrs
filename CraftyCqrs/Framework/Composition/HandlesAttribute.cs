﻿
using System;

namespace CraftyCqrs.Framework.Composition
{
    /// <summary>
    /// Declares the class as a command handler that 
    /// handles the specified command
    /// </summary>
    public class HandlesAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the HandlesAttribute class.
        /// </summary>
        /// <param name="commandHandled">The name of the command that is handled by this class</param>
        public HandlesAttribute(string commandHandled)
        {
            this.CommandHandled = commandHandled;
        }
        
        /// <summary>
        /// The name of the command that is handled by this class
        /// </summary>
        public string CommandHandled { get; private set; }
    }
}
