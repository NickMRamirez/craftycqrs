﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CraftyCqrs.Framework.Composition
{
    internal class CommandHandlerClassFinder : ICommandHandlerClassFinder
    {
        private IAssemblyFinder assemblyFinder;

        /// <summary>
        /// Initializes a new instance of the CommandHandlerClassFinder class.
        /// </summary>
        public CommandHandlerClassFinder() : this(new AssemblyFinder())
        {
        }

        /// <summary>
        /// Initializes a new instance of the CommandHandlerClassFinder class.
        /// </summary>
        /// <param name="assemblyFinder">An instance of IAssemblyFinder for finding assemblies in 
        /// the current directory and its subdirectories</param>
        public CommandHandlerClassFinder(IAssemblyFinder assemblyFinder)
        {
            this.assemblyFinder = assemblyFinder;
        }

        /// <summary>
        /// Gets a collection of command handler types exposed by the 
        /// asemblies in the current directory and all subdirectories
        /// </summary>
        /// <returns>A collection of command handler types</returns>
        public IEnumerable<Type> GetCommandHandlerTypes()
        {
            var assemblies = this.assemblyFinder.GetAssemblies();

            foreach (var assm in assemblies)
            {
                Assembly loadedAssembly = Assembly.LoadFile(assm);

                var commands =
                    from type in loadedAssembly.ExportedTypes
                    where typeof(CommandHandler).IsAssignableFrom(type)
                    select type;

                foreach (var command in commands)
                {
                    yield return command;
                }
            }
        }
    }
}