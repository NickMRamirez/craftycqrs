﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CraftyCqrs.Framework
{
    using System;

    /// <summary>
    /// Defines an exception for when a command does not
    /// have an associated command handler
    /// </summary>
    public class UndefinedCommandException : ApplicationException
    {
        /// <summary>
        /// Initializes a new instance of the UndefinedCommandException class.
        /// </summary>
        /// <param name="commandName">The name of the command</param>
        public UndefinedCommandException(string commandName)
            : base(string.Format("The {0} has no associated handler. Be sure that the handler has the Handles attribute and that it specifies the fully qualified name of the command class.", commandName))
        {
        }
    }
}
