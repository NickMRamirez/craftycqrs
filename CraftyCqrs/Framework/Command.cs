﻿
namespace CraftyCqrs.Framework
{
    /// <summary>
    /// Represents an action to take within the domain
    /// </summary>
    public abstract class Command
    {
    }
}