﻿
namespace CraftyCqrs.Framework
{
    /// <summary>
    /// Wraps a command for easier consumption by the command router
    /// </summary>
    public class CommandEnvelope
    {
        /// <summary>
        /// Initializes a new instance of the CommandEnvelope class.
        /// </summary>
        /// <param name="command">The command to wrap</param>
        public CommandEnvelope(dynamic command)
        {
            this.Command = command;
        }
        
        /// <summary>
        /// Gets the command wrapped by this envelope
        /// </summary>
        public dynamic Command { get; private set; }
    }
}