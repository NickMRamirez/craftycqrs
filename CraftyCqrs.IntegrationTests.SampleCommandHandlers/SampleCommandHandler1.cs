﻿
using CraftyCqrs.Framework;
using CraftyCqrs.Framework.Composition;
using CraftyCqrs.IntegrationTests.SampleDomainModel;

namespace CraftyCqrs.IntegrationTests.SampleCommandHandlers
{
    [Handles("CraftyCqrs.IntegrationTests.SampleCommands.SampleCommand1")]
    public class SampleCommandHandler1 : CommandHandler
    {
        public override void Execute(dynamic command)
        {
            SampleDomainModelClass.StringToSet = "test value";
        }
    }
}