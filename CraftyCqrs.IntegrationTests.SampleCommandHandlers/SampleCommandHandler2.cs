﻿
using CraftyCqrs.Framework;
using CraftyCqrs.Framework.Composition;
using CraftyCqrs.IntegrationTests.SampleDomainModel;

namespace CraftyCqrs.IntegrationTests.SampleCommandHandlers
{
    [Handles("CraftyCqrs.IntegrationTests.SampleCommands.SampleCommand2")]
    public class SampleCommandHandler2 : CommandHandler
    {
        public override void Execute(dynamic command)
        {
            SampleDomainModelClass.StringToSet = command.TestProperty1;
        }
    }
}
