﻿/*
 * Demonstrates using command and handlers to update objects in the domain model
 */

using CraftyCqrs.Framework;
using CraftyCqrs.Framework.Composition;
using CraftyCqrs.Samples.SimpleConsoleApp.Domain;
using System;
using System.Collections.Generic;

namespace CraftyCqrs.Samples.SimpleConsoleApp
{
    public class Program
    {
        static void Main(string[] args)
        {
            var cmdEnvelope = new CommandEnvelope(new OrderFrenchToastCommand(3, true, "add whip cream"));

            var router = new CommandRouter();
            router.Send(cmdEnvelope);

            Console.WriteLine(
                "Order details in model:\n Toast slices: {0}\n Syrup: {1}\n Special Instructions: {2}",
                DinerDomainModelAggregate.Orders[0].ToastSlices,
                DinerDomainModelAggregate.Orders[0].Syrup,
                DinerDomainModelAggregate.Orders[0].SpecialInstructions);
        }
    }
}